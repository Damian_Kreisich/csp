import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Futoshiki extends BoardCSP{
    private List<FutoshikiConstraint> additionalConstraints;

    public Futoshiki(int n) {
        super(n);
        additionalConstraints = new ArrayList<>();
    }
    //getPossibleValues returns null when there was a starter value
    //getPossibleValues returns empty list when there is no value matching constraints


    @Override
    protected void countConstraints() {
        int count = 0;
        for(int i = 0; i < board.size(); i++) {
            for (int j = 0; j < board.size(); j++) {
                for (FutoshikiConstraint con : additionalConstraints) {
                    if (con.isConstrained(i, j)){
                        count++;
                    }
                }
                board.get(i).get(j).setNumberOfConstraints(count);
                count = 0;
            }
        }
    }

    public void addAdditionalConstraint(String lower, String greater ){ //lower and greater are short strings in manner of A1 - C3 - B4 etc.
        additionalConstraints.add(new FutoshikiConstraint(distinguishToNumbers(lower), distinguishToNumbers(greater)));
    }
    private int[] distinguishToNumbers(String rel){
        int [] result = new int [2];
        result[0] = rel.charAt(0) - Constants.FIRST_ASCII_CAPITAL_LETTER;
        result[1] = rel.charAt(1) - Constants.NUMBER_ONE_ASCII_VALUE;
        return result;
    }

    //possibly can be done better -- temporary solution
    protected boolean checkSpecialConstraints(int row, int column, int value){
        boolean res = true;
        int lowerValue;
        int greaterValue;
        for(FutoshikiConstraint fc : additionalConstraints){
            if (fc.getLowerColumn() == column && fc.getLowerRow() == row){
                res = value != values.size();
                if (!res){
                    return res;
                }
                greaterValue = board.get(fc.getGreaterRow()).get(fc.getGreaterColumn()).getValue();
                if(greaterValue != 0){
                    if (greaterValue < value){
                        return false;
                    }
                }
            }
            if (fc.getGreaterColumn() == column && fc.getGreaterRow() == row){
                res = value != 1;
                if (!res){
                    return res;
                }
                lowerValue = board.get(fc.getLowerRow()).get(fc.getLowerColumn()).getValue();
                if(lowerValue != 0){
                    if (lowerValue > value){
                        return false;
                    }
                }
            }
        }
        return res;
    }

    public List<FutoshikiConstraint> getAdditionalConstraints() {
        return additionalConstraints;
    }
}