import java.util.ArrayList;
import java.util.List;

public class SkyscrapperConstraint {
    private List<Integer> values;

    public SkyscrapperConstraint(List <Integer> values){
        this.values = new ArrayList<>();
        this.values.addAll(values);
    }

    public List<Integer> getValues() {
        return values;
    }

    public void setValues(List<Integer> values) {
        this.values = values;
    }

    @Override
    public String toString() {
        return "" + values;
    }
}
