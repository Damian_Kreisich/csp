import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SkyscrapperLoader implements Loader {
    private BufferedReader reader;

    public SkyscrapperLoader(String fileName){
        try {
            reader = new BufferedReader(new FileReader(fileName));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Skyscrapper createBoard() {
        Skyscrapper game = null;
        try{
            game = new Skyscrapper(Integer.parseInt(reader.readLine()));
            String [] actualStringValues = null;
            char position = ' ';
            List<Integer> values = new ArrayList<>();
            while (reader.ready()){
                actualStringValues = reader.readLine().split(";");
                for(int i = 0; i < actualStringValues.length; i++){
                    if (i == 0){
                        position = actualStringValues[i].charAt(0);
                    }
                    else{
                        values.add(Integer.parseInt(actualStringValues[i]));
                    }
                }
                game.addConstraint(position, values);
                values = new ArrayList<>();
            }
            reader.close();
        }
        catch (IOException e){
            e.printStackTrace();
        }
        return game;
    }
}
