public interface MySequence {
    void nextVal();
    void previousVal();
    int getRow();
    int getColumn();
    int getSize();
}
