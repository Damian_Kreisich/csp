import java.util.List;

public interface CSP {
     void resetValue(int row, int column);
     List<Integer> getPossibleValues(int row, int column);
     void setSingleBoardValue(int row, int column, int value);
     boolean checkWinning();
}
