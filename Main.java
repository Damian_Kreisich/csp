public class Main {
    private static void bothProblems(int nr){
        System.out.println("Problems "+ nr +"_0: \n");
        FutoshikiLoader loader = new FutoshikiLoader("C:\\Users\\Master\\Desktop\\Studia\\SI\\CSP\\Badawcze\\test_futo_"+ nr +"_0.txt");
        SkyscrapperLoader loader1 = new SkyscrapperLoader("C:\\Users\\Master\\Desktop\\Studia\\SI\\CSP\\Badawcze\\test_sky_"+ nr +"_0.txt");
        Futoshiki game = loader.createBoard();
        Skyscrapper game1 = loader1.createBoard();

//        MySequence seq = new ClassicSequence(0, -1, game.board.size());
//        MySequence seq1 = new ClassicSequence(0, -1, game1.board.size());
        MySequence seq = new MetaSequence(game, 0, 0);
        MySequence seq1 = new MetaSequence(game1, 0, 0);
        CSPAlgorithm alg = new Backtracking(game, seq);
        CSPAlgorithm alg1 = new Backtracking(game1, seq1);

        alg.evaluateProblem();
        alg1.evaluateProblem();

        System.out.println("Problems "+ nr +"_1: \n");
        FutoshikiLoader loader2 = new FutoshikiLoader("C:\\Users\\Master\\Desktop\\Studia\\SI\\CSP\\Badawcze\\test_futo_"+ nr +"_1.txt");
        SkyscrapperLoader loader3 = new SkyscrapperLoader("C:\\Users\\Master\\Desktop\\Studia\\SI\\CSP\\Badawcze\\test_sky_"+ nr +"_1.txt");
        Futoshiki game2 = loader2.createBoard();
        Skyscrapper game3 = loader3.createBoard();

//        MySequence seq2 = new ClassicSequence(0, -1, game2.board.size());
//        MySequence seq3 = new ClassicSequence(0, -1, game3.board.size());
        MySequence seq2 = new MetaSequence(game2, 0, 0);
        MySequence seq3 = new MetaSequence(game3, 0, 0);
        CSPAlgorithm alg2 = new Backtracking(game2, seq2);
        CSPAlgorithm alg3 = new Backtracking(game3, seq3);

        alg2.evaluateProblem();
        alg3.evaluateProblem();

        System.out.println("Problems "+ nr +"_2: \n");
        FutoshikiLoader loader4 = new FutoshikiLoader("C:\\Users\\Master\\Desktop\\Studia\\SI\\CSP\\Badawcze\\test_futo_"+ nr +"_2.txt");
        SkyscrapperLoader loader5 = new SkyscrapperLoader("C:\\Users\\Master\\Desktop\\Studia\\SI\\CSP\\Badawcze\\test_sky_"+ nr +"_2.txt");
        Futoshiki game4 = loader4.createBoard();
        Skyscrapper game5 = loader5.createBoard();

//        MySequence seq4 = new ClassicSequence(0, -1, game4.board.size());
//        MySequence seq5 = new ClassicSequence(0, -1, game5.board.size());
        MySequence seq4 = new MetaSequence(game4, 0, 0);
        MySequence seq5 = new MetaSequence(game5, 0, 0);
        CSPAlgorithm alg4 = new Backtracking(game4, seq4);
        CSPAlgorithm alg5 = new Backtracking(game5, seq5);

        alg4.evaluateProblem();
        alg5.evaluateProblem();

        System.out.println("Skycrapper "+ nr +"_3: \n");
        SkyscrapperLoader loader6 = new SkyscrapperLoader("C:\\Users\\Master\\Desktop\\Studia\\SI\\CSP\\Badawcze\\test_sky_"+ nr +"_3.txt");
        Skyscrapper game6 = loader6.createBoard();

        MySequence seq6 = new MetaSequence(game6, 0, 0);
//        MySequence seq6 = new ClassicSequence(0, -1, game6.board.size());

        CSPAlgorithm alg6 = new Backtracking(game6, seq6);

        alg6.evaluateProblem();

        System.out.println("Skycrapper "+ nr +"_4: \n");
        SkyscrapperLoader loader7 = new SkyscrapperLoader("C:\\Users\\Master\\Desktop\\Studia\\SI\\CSP\\Badawcze\\test_sky_"+ nr +"_4.txt");
        Skyscrapper game7 = loader7.createBoard();

        MySequence seq7 = new MetaSequence(game7, 0, 0);
//        MySequence seq7 = new ClassicSequence(0, -1, game7.board.size());

        CSPAlgorithm alg7 = new Backtracking(game7, seq7);

        alg7.evaluateProblem();
    }
    private static void onlyFuto(int nr){
        System.out.println("Backtracking");
        System.out.println("Futoshiki "+ nr +"_0: \n");
        FutoshikiLoader loader = new FutoshikiLoader("C:\\Users\\Master\\Desktop\\Studia\\SI\\CSP\\Badawcze\\test_futo_"+ nr +"_0.txt");
        Futoshiki game = loader.createBoard();

        MySequence seq = new MetaSequence(game, 0, 0);
        //MySequence seq = new ClassicSequence(0, -1, game.board.size());

        CSPAlgorithm alg = new Backtracking(game, seq);

        alg.evaluateProblem();

        System.out.println("Futoshiki "+ nr +"_1: \n");
        FutoshikiLoader loader1 = new FutoshikiLoader("C:\\Users\\Master\\Desktop\\Studia\\SI\\CSP\\Badawcze\\test_futo_"+ nr +"_1.txt");
        Futoshiki game1 = loader1.createBoard();

        MySequence seq1 = new MetaSequence(game1, 0, 0);
        //MySequence seq1 = new ClassicSequence(0, -1, game1.board.size());

        CSPAlgorithm alg1 = new Backtracking(game1, seq1);

        alg1.evaluateProblem();

        System.out.println("Futoshiki "+ nr +"_2: \n");
        FutoshikiLoader loader2 = new FutoshikiLoader("C:\\Users\\Master\\Desktop\\Studia\\SI\\CSP\\Badawcze\\test_futo_"+ nr +"_2.txt");
        Futoshiki game2 = loader2.createBoard();

        MySequence seq2 = new MetaSequence(game2, 0, 0);
        //MySequence seq2 = new ClassicSequence(0, -1, game2.board.size());

        CSPAlgorithm alg2 = new Backtracking(game2, seq2);

        alg2.evaluateProblem();

        System.out.println("Forward checking");
        System.out.println("Futoshiki "+ nr +"_0: \n");
        FutoshikiLoader loader3 = new FutoshikiLoader("C:\\Users\\Master\\Desktop\\Studia\\SI\\CSP\\Badawcze\\test_futo_"+ nr +"_0.txt");
        Futoshiki game3 = loader3.createBoard();

        MySequence seq3 = new MetaSequence(game3, 0, 0);
       // MySequence seq3 = new ClassicSequence(0, -1, game3.board.size());

        CSPAlgorithm alg3 = new ForwardChecking(game3, seq3);

        alg3.evaluateProblem();

        System.out.println("Futoshiki "+ nr +"_1: \n");
        FutoshikiLoader loader4 = new FutoshikiLoader("C:\\Users\\Master\\Desktop\\Studia\\SI\\CSP\\Badawcze\\test_futo_"+ nr +"_1.txt");
        Futoshiki game4 = loader4.createBoard();

        MySequence seq4 = new MetaSequence(game4, 0, 0);
       // MySequence seq4 = new ClassicSequence(0, -1, game4.board.size());

        CSPAlgorithm alg4 = new ForwardChecking(game4, seq4);

        alg4.evaluateProblem();

        System.out.println("Futoshiki "+ nr +"_2: \n");
        FutoshikiLoader loader5 = new FutoshikiLoader("C:\\Users\\Master\\Desktop\\Studia\\SI\\CSP\\Badawcze\\test_futo_"+ nr +"_2.txt");
        Futoshiki game5 = loader5.createBoard();

        MySequence seq5 = new MetaSequence(game5, 0, 0);
        //MySequence seq5 = new ClassicSequence(0, -1, game5.board.size());

        CSPAlgorithm alg5 = new ForwardChecking(game5, seq5);

        alg5.evaluateProblem();
    }
    public static void main(String[] args) {
        for (int i = 4; i<=6; i++){
            Main.bothProblems(i);
        }
        for (int i = 7; i<=8; i++){
            Main.onlyFuto(i);
        }

    }
}
