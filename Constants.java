public class Constants {
    public static final int FIRST_ASCII_CAPITAL_LETTER = 65;
    public static final int NUMBER_ONE_ASCII_VALUE = 49;

    public static final int SKYSCRAPPER_TOP_CONSTRAINT = 0;
    public static final int SKYSCRAPPER_BOTTOM_CONSTRAINT = 1;
    public static final int SKYSCRAPPER_LEFT_CONSTRAINT = 2;
    public static final int SKYSCRAPPER_RIGHT_CONSTRAINT = 3;
    public static final int SKYSCRAPPER_POSSIBLE_CONSTRAINTS = 4;
}
