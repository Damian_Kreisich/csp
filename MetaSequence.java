import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class MetaSequence implements MySequence {
    private BoardCSP problem;
    private int actualRow;
    private int actualColumn;
    private List<List<List<Integer>>> boardPossibleValues;
    private LinkedList<Integer> previousPositions;

    public MetaSequence(BoardCSP problem, int startingRow, int startingColumn){
        this.problem = problem;
        actualRow = startingRow;
        actualColumn = startingColumn;
        previousPositions = new LinkedList<>();

        this.problem.countConstraints();
    }

    @Override
    public void nextVal() {
        if (boardPossibleValues != null){
            addToHistory();
        }
        preparePossibleVariables();
        chooseVariable();

    }

    private void addToHistory(){
        previousPositions.add(actualRow);
        previousPositions.add(actualColumn);
    }

    private void getFromHistory(){
        actualColumn = previousPositions.getLast();
        previousPositions.removeLast();
        actualRow = previousPositions.getLast();
        previousPositions.removeLast();
    }

    private void preparePossibleVariables(){
        int size = problem.board.size();
        if (boardPossibleValues == null){
            boardPossibleValues = new ArrayList<>();

            for (int i = 0; i < size; i++){
                boardPossibleValues.add(new ArrayList<>());
                for (int j = 0; j < size; j++){
                    boardPossibleValues.get(i).add(problem.getPossibleValues(i, j));
                }
            }
        }
        else{
            GameValue actual;
            for(int i = 0; i < size; i++){
                actual = problem.board.get(i).get(actualColumn);
                if (actual != null) {
                    if (actual.getValue() == 0) {
                        boardPossibleValues.get(i).set(actualColumn, problem.getPossibleValues(i, actualColumn));
                    }
                }
                actual = problem.board.get(actualRow).get(i);
                if (actual != null) {
                    if (actual.getValue() == 0) {
                        boardPossibleValues.get(actualRow).set(i, problem.getPossibleValues(actualRow, i));
                    }
                }
            }
        }
    }
    private void chooseVariable(){
        int size = problem.board.size();
        int actualBestSize = 1000;
        int mostConstraint = 0;
        int actualConstraint = 0;
        int actualMeasure = 0;
        List <Integer> actualPossibleValues = null;
        for (int i = 0; i < size; i++) {

            for (int j = 0; j < size; j++) {
                actualPossibleValues = boardPossibleValues.get(i).get(j);
                if (actualPossibleValues != null && problem.board.get(i).get(j).getValue() == 0) {
                    actualMeasure = actualPossibleValues.size();
                    actualConstraint = problem.board.get(i).get(j).getNumberOfConstraints();
                    if (actualMeasure < actualBestSize) {
                        actualBestSize = actualMeasure;
                        actualRow = i;
                        actualColumn = j;
                        mostConstraint = 0;
                    } else if (actualMeasure == actualBestSize) {
                        if (mostConstraint < actualConstraint) {
                            actualBestSize = actualMeasure;
                            actualRow = i;
                            actualColumn = j;
                            mostConstraint = actualConstraint;
                        }
                    }
                }
            }
        }
    }

    @Override
    public void previousVal() {
            getFromHistory();
    }

    @Override
    public int getRow() {
        return actualRow;
    }

    @Override
    public int getColumn() {
        return actualColumn;
    }

    @Override
    public int getSize() {
        return problem.board.size();
    }
}
