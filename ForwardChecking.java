import java.util.List;

public class ForwardChecking extends  CSPAlgorithm{
    public ForwardChecking(BoardCSP problem, MySequence seq){
        super(problem, seq);
    }

    @Override
    void evaluateProblem() {
        long startTime = System.currentTimeMillis();
        try {

            chooseNextElementSequentially();
            while (!problem.checkWinning()) {
                if (!forwardCheck(values.getLast().getActualValue())) {
                    if (values.getLast().getPossibleValues().size() == 0 || !values.getLast().next()) {
                        backtrack();
                    }
                } else {
                    chooseNextElementSequentially();
                }
                iterations++;
            }
            long endTime = System.currentTimeMillis();
            time = (((endTime - startTime) ) / 1000) ; //time in milis
            System.out.println("Time elapsed in seconds: " + time);
            System.out.println("Solved problem: \n" + problem);
            System.out.println("Number of iterations: " + iterations);
            System.out.println("--------------------------------------------------");
        }
        catch (Exception e){
            time = (((System.currentTimeMillis() - startTime ) ) / 1000) ; //time in milis
            System.out.println("Time elapsed in seconds: " + time);
            System.out.println("Number of iterations: " + iterations);
            System.out.println("Nie ma rozwiązań");
        }
    }
    private boolean forwardCheck(int value){
        if (value == 0){
            return false;
        }
        boolean res = true;

        int desiredRow = seq.getRow();
        int desiredColumn = seq.getColumn();
        problem.setSingleBoardValue(desiredRow, desiredColumn, value);
        for (int i = 0; i < seq.getSize(); i++) {
            if (problem.getBoard().get(i).get(desiredColumn).getValue() == 0) {
                if(problem.getPossibleValues(i, desiredColumn).size() == 0) {
                    res = false;
                }
            }
            if (problem.getBoard().get(desiredRow).get(i).getValue() == 0){
                if (problem.getPossibleValues(desiredRow, i).size() == 0){
                    res = false;
                }
            }
        }
        if (!res)
            problem.resetValue(desiredRow, desiredColumn);
        return res;
    }
//    private void slicePossibleValuesAndSort(){
//        for (Integer i: values.getLast().getPossibleValues()) {
//            if (forwardCheck())
//        }
//    }
}
