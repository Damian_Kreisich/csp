public class FutoshikiConstraint {
    private int greaterColumn;
    private int greaterRow;
    private int lowerColumn;
    private int lowerRow;

    public FutoshikiConstraint(int [] lowerPos, int [] greaterPos) {
        this.greaterColumn = greaterPos[1];
        this.greaterRow = greaterPos[0];
        this.lowerColumn = lowerPos[1];
        this.lowerRow = lowerPos[0];
    }

    @Override
    public String toString() {
        return "[" + lowerRow + ", " + lowerColumn + "] < [" + greaterRow + ", " + greaterColumn + "]";
    }

    public int getGreaterColumn() {
        return greaterColumn;
    }

    public int getGreaterRow() {
        return greaterRow;
    }

    public int getLowerColumn() {
        return lowerColumn;
    }

    public int getLowerRow() {
        return lowerRow;
    }

    public boolean isConstrained(int row, int column){
        return (row == greaterRow && column == greaterColumn) || (row == lowerRow && column == lowerColumn);
    }
    public boolean isLowerConstrained(int row, int column){
        return (row == lowerRow && column == lowerColumn);
    }
    public boolean isHigherConstrained(int row, int column){
        return (row == greaterRow && column == greaterColumn);
    }
}
