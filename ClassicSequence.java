public class ClassicSequence implements MySequence {
    private int actualRow;
    private int actualColumn;
    private int size;

    public ClassicSequence(int actualRow, int actualColumn, int size) {
        this.actualRow = actualRow;
        this.actualColumn = actualColumn;
        this.size = size;
    }

    public void nextVal(){
        if (actualColumn < size-1 || actualRow < size-1) {
            actualColumn++;
            if (actualColumn >= size) {
                actualRow++;
                actualColumn = 0;
            }
        }
    }

    public void previousVal(){
        if (actualColumn > 0 || actualRow > 0) {
            actualColumn--;
            if (actualColumn <= -1) {
                actualRow--;
                actualColumn = size - 1;
            }
        }
    }
    public int getRow() {
        return actualRow;
    }

    public int getColumn() {
        return actualColumn;
    }

    public int getSize() {
        return size;
    }
}
