import java.util.LinkedList;
import java.util.List;

public class Variable {
    private List<Integer> possibleValues;
    private int actualValue;

    public Variable(List<Integer> possibleValues) {
        this.possibleValues = possibleValues;
        if(possibleValues.size() > 0)
            this.actualValue = ((LinkedList<Integer>)this.possibleValues).getFirst();
        else {
            this.actualValue = 0;
        }
    }

    public boolean next() {
        ((LinkedList<Integer>) possibleValues).removeFirst();
        if (possibleValues.size() > 0){
            actualValue = ((LinkedList<Integer>) possibleValues).getFirst();
            return true;
        }
        else {
            actualValue = 0;
            return false;
        }

    }

    public int getActualValue() {
        return actualValue;
    }

    public void setActualValue(int actualValue) {
        this.actualValue = actualValue;
    }

    public List<Integer> getPossibleValues() {
        return possibleValues;
    }

    public void setPossibleValues(List<Integer> possibleValues) {
        this.possibleValues = possibleValues;
    }

}
