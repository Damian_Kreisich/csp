import java.util.Objects;

public class GameValue {
    private int value;
    private boolean starter;
    private int numberOfConstraints;
    public GameValue(int value){
        this.value = value;
        starter = false;
    }

    public void setStarter(boolean starter) {
        this.starter = starter;
    }

    public int getValue() {
        return value;
    }

    public boolean isStarter() {
        return starter;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        return value == ((GameValue)o).value;
    }

    @Override
    public String toString() {
        return "" + value;
//        return "" + starter;
    }

    public int getNumberOfConstraints() {
        return numberOfConstraints;
    }

    public void setNumberOfConstraints(int numberOfConstraints) {
        this.numberOfConstraints = numberOfConstraints;
    }
}
