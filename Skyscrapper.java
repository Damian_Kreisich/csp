import java.util.ArrayList;
import java.util.List;

public class Skyscrapper extends BoardCSP {
    private List<SkyscrapperConstraint> constraints;

    public Skyscrapper(int n) {
        super(n);
        constraints = new ArrayList<>();
        for (int i = 0; i < Constants.SKYSCRAPPER_POSSIBLE_CONSTRAINTS; i++) {
            constraints.add(null);
        }
    }

    @Override
    protected void countConstraints() {
        GameValue gv;
        for(int i = 0; i < board.size(); i++){
            for (int j = 0; j < board.size(); j++) {
                gv = board.get(i).get(j);
                if(constraints.get(Constants.SKYSCRAPPER_TOP_CONSTRAINT).getValues().get(j) != 0){
                    gv.setNumberOfConstraints(gv.getNumberOfConstraints()+1);
                }
                if(constraints.get(Constants.SKYSCRAPPER_BOTTOM_CONSTRAINT).getValues().get(j) != 0){
                    gv.setNumberOfConstraints(gv.getNumberOfConstraints()+1);
                }
                if(constraints.get(Constants.SKYSCRAPPER_LEFT_CONSTRAINT).getValues().get(i) != 0){
                    gv.setNumberOfConstraints(gv.getNumberOfConstraints()+1);
                }
                if(constraints.get(Constants.SKYSCRAPPER_RIGHT_CONSTRAINT).getValues().get(i) != 0){
                    gv.setNumberOfConstraints(gv.getNumberOfConstraints()+1);
                }
            }
        }
    }

    @Override
    protected boolean checkSpecialConstraints(int row, int column, int value) {
        boolean res = true;
        //check top constraint

        res = checkTopConstraint(row, column, value) &&
                checkBottomConstraint(row, column, value) &&
                checkLeftConstraint(row, column, value) &&
                checkRightConstraint(row, column, value);

        return res;
    }
    private boolean checkTopConstraint(int row, int column, int value){
        int topConstraint = constraints.get(Constants.SKYSCRAPPER_TOP_CONSTRAINT).getValues().get(column);
        int actualMax = 0;
        int actualValue = 0;
        int count = 0;

        if (topConstraint == 0){
            return true;
        }
        if (topConstraint == 1 && row==0){
            return value==values.get(values.size()-1);
        }
        if (topConstraint == values.get(values.size()-1) && row==0){
            return value==1;
        }

        for (int i = 0; i < board.size(); i++) {
            if (count > topConstraint){
                return false;
            }
            if (i != row) {
                actualValue = board.get(i).get(column).getValue();
            }
            else {
                actualValue = value;
            }
            if (actualValue != 0) {
                if (actualValue > actualMax){
                    actualMax = actualValue;
                    count++;
                }
            }
            else {
                return true;
            }
        }
        return count == topConstraint;
    }
    private boolean checkBottomConstraint(int row, int column, int value){
        int bottomConstraint = constraints.get(Constants.SKYSCRAPPER_BOTTOM_CONSTRAINT).getValues().get(column);
        int actualMax = 0;
        int actualValue = 0;
        int count = 0;
        if (bottomConstraint == 0){
            return true;
        }
        if (bottomConstraint == 1 && row==values.size()-1){
            return value==values.get(values.size()-1);
        }
        if (bottomConstraint == values.get(values.size()-1) && row==values.size()-1){
            return value==1;
        }

        for (int i = board.size()-1; i >= 0; i--) {
            if (count > bottomConstraint){
                return false;
            }
            if (i != row) {
                actualValue = board.get(i).get(column).getValue();
            }
            else {
                actualValue = value;
            }
            if (actualValue != 0) {
                if (actualValue > actualMax) {
                    actualMax = actualValue;
                    count++;
                }
            }
            else {
                return true;
            }
        }
        return count == bottomConstraint;
    }
    private boolean checkLeftConstraint(int row, int column, int value){
        int leftConstraint = constraints.get(Constants.SKYSCRAPPER_LEFT_CONSTRAINT).getValues().get(row);
        int actualMax = 0;
        int actualVal = 0;
        int count = 0;
        if (leftConstraint == 0){
            return true;
        }
        if (leftConstraint == 1 && column==0){
            return value==values.get(values.size()-1);
        }
        if (leftConstraint == values.get(values.size()-1) && column==0){
            return value==1;
        }

        for (int i = 0; i < board.size(); i++) {
            if (count > leftConstraint){
                return false;
            }
            if (i != column) {
                actualVal = board.get(row).get(i).getValue();
            }
            else {
                actualVal = value;
            }
            if (actualVal != 0){
                if (actualVal > actualMax){
                    actualMax = actualVal;
                    count++;
                }
            }
            else {
                return true;
            }
        }
        return count == leftConstraint;
    }
    private boolean checkRightConstraint(int row, int column, int value){
        int rightConstraint = constraints.get(Constants.SKYSCRAPPER_RIGHT_CONSTRAINT).getValues().get(row);
        int actualMax = 0;
        int actualValue = 0;
        int count = 0;
        if (rightConstraint == 0){
            return true;
        }
        if (rightConstraint == 1 && column==values.size()-1){
            return value==values.get(values.size()-1);
        }
        if (rightConstraint == values.get(values.size()-1) && column==values.size()-1){
            return value==1;
        }


        for (int i = board.size()-1; i >= 0; i--) {
            if (count > rightConstraint){
                return false;
            }
            if (i != column) {
                actualValue = board.get(row).get(i).getValue();
            }
            else {
                actualValue = value;
            }
            if (actualValue != 0) {
                if (actualValue > actualMax) {
                    actualMax = actualValue;
                    count++;
                }
            }
            else {
                return true;
            }
        }
        return count == rightConstraint;
    }
    public void addConstraint(char pos, List<Integer> values){
        switch(pos){
            case 'G' :
                constraints.set(Constants.SKYSCRAPPER_TOP_CONSTRAINT, new SkyscrapperConstraint(values));
                break;
            case 'D' :
                constraints.set(Constants.SKYSCRAPPER_BOTTOM_CONSTRAINT, new SkyscrapperConstraint(values));
                break;
            case 'L':
                constraints.set(Constants.SKYSCRAPPER_LEFT_CONSTRAINT, new SkyscrapperConstraint(values));
                break;
            case 'P':
                constraints.set(Constants.SKYSCRAPPER_RIGHT_CONSTRAINT, new SkyscrapperConstraint(values));
                break;
        }
    }


    public String toString(){
        String result = "( )";
        result += constraints.get(Constants.SKYSCRAPPER_TOP_CONSTRAINT).toString();
        result = result.replace('[', '(');
        result = result.replace(']', ')');
        result += "( )\n";
        for (int i = 0; i < board.size(); i++) {
            result += "(" + constraints.get(Constants.SKYSCRAPPER_LEFT_CONSTRAINT).getValues().get(i)+ ")"
                    +board.get(i).toString() +
                    "(" + constraints.get(Constants.SKYSCRAPPER_RIGHT_CONSTRAINT).getValues().get(i)+ ")" +"\n";
        }
        result += "( )";
        result += constraints.get(Constants.SKYSCRAPPER_BOTTOM_CONSTRAINT).toString().replace('[', '(').replace(']', ')');
        result += "( )\n";
        return result;
    }
}
