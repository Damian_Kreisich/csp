import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class FutoshikiLoader implements Loader{
    private BufferedReader reader;
    public FutoshikiLoader(String fileName){
        try {
            reader = new BufferedReader(new FileReader(fileName));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
    public Futoshiki createBoard(){
        Futoshiki game = null;
        try {
            int n = Integer.parseInt(reader.readLine());

            reader.readLine(); //Avoid one line

            game = new Futoshiki(n);
            String [] actualLineValues;
            int parsedValue;
            GameValue gv = null;
            for(ArrayList<GameValue> row : game.getBoard()){
                actualLineValues = reader.readLine().split(";");
                for (int i = 0; i < row.size(); i++) {
                    parsedValue = Integer.parseInt(actualLineValues[i]);
                    if (parsedValue != 0) {
                        gv = new GameValue(parsedValue);
                        gv.setStarter(true);
                        row.set(i, gv);
                        game.actualState++;
                    }
                }
            }

            reader.readLine();//avoid one line

            while (reader.ready()){
                actualLineValues = reader.readLine().split(";");
                game.addAdditionalConstraint(actualLineValues[0], actualLineValues[1]);
            }
            reader.close();
        }
        catch (IOException e){
            e.printStackTrace();
        }

        return game;
    }
}
