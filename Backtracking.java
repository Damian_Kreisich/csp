import java.util.LinkedList;
import java.util.List;

public class Backtracking extends CSPAlgorithm {
    public Backtracking(BoardCSP problem, MySequence seq){
        super(problem, seq);
    }


    public void evaluateProblem() {
        long startTime = System.currentTimeMillis();
        try {
            Variable lastVariable;


            while (!problem.checkWinning()) {

                chooseNextElementSequentially();

                while (values.getLast().getActualValue() == 0) {
                    backtrack();
                }


                problem.setSingleBoardValue(seq.getRow(), seq.getColumn(), values.getLast().getActualValue());
                iterations++;
            }
            long endTime = System.currentTimeMillis();
            time = (((endTime - startTime) ) / 1000) ; //time in milis
            System.out.println("Time elapsed in seconds: " + time);
            System.out.println("Solved problem: \n" + problem);
            System.out.println("Number of iterations: " + iterations);
            System.out.println("--------------------------------------------------");
        }
        catch (Exception e){
            time = (((System.currentTimeMillis() - startTime )) / 1000) ; //time in milis
            System.out.println("Time elapsed in seconds: " + time);
            System.out.println("Number of iterations: " + iterations);
            System.out.println("Nie ma rozwiązań");
        }
    }
}
