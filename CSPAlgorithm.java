import java.util.LinkedList;

abstract class CSPAlgorithm {
    protected LinkedList<Variable> values;
    protected BoardCSP problem;
    protected MySequence seq;
    protected long iterations;
    protected long time;

    abstract void evaluateProblem();

    public CSPAlgorithm(BoardCSP problem, MySequence seq){
        values = new LinkedList<>();
        this.problem = problem;
        this.seq = seq;
    }

    protected void chooseNextElementSequentially(){
        seq.nextVal();
        while (problem.board.get(seq.getRow()).get(seq.getColumn()).isStarter()){
            seq.nextVal();
        }

        values.add(new Variable(problem.getPossibleValues(seq.getRow(), seq.getColumn())));
    }

    protected void backtrack(){
        values.removeLast();

        seq.previousVal();

        while (problem.board.get(seq.getRow()).get(seq.getColumn()).isStarter()){
            seq.previousVal();
        }
        problem.resetValue(seq.getRow(), seq.getColumn());

        Variable actualLast = values.getLast();
        actualLast.next();
    }

}
