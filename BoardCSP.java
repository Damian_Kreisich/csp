import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

abstract class BoardCSP {
    protected List<ArrayList<GameValue>> board;
    protected List<Integer> values; //1 to values
    protected int actualState; //actualState|boardSize
    protected int boardSize;

    abstract protected boolean checkSpecialConstraints(int row, int column, int value);
    abstract protected void countConstraints();

    private boolean checkConstraints(int row, int column, int value){
        boolean res = true;
        res = !board.get(row).contains(new GameValue(value));
        if (res){
            for (int i = 0; i < board.size(); i++){
                if (board.get(i).get(column).getValue() == value){
                    return false;
                }
            }
            res = checkSpecialConstraints(row, column, value);
        }
        return res;
    }

    public List<Integer> getPossibleValues(int row, int column){
        List<Integer> possibleValues = new LinkedList<>();
        int actualValue;
        if (!board.get(row).get(column).isStarter()) {
            for (int i = 0; i < values.size(); i++) {
                actualValue = values.get(i);
                if (checkConstraints(row, column, actualValue)) {
                    possibleValues.add(actualValue);
                }
            }
        }
        else{
            return null;
        }
        return possibleValues;
    }

    public BoardCSP(int n){
        boardSize = (int) Math.floor(Math.pow(n/1.0 , 2));
        values = IntStream.rangeClosed(1, n).boxed().collect(Collectors.toList());
        actualState = 0;

        board = new ArrayList<>();
        for (int i = 0; i < n; i++){
            board.add(new ArrayList<>());
            for (int j = 0; j < n; j++){
                board.get(i).add(new GameValue(0));
            }
        }
    }
    public void resetValue(int row, int column){
        GameValue actual = board.get(row).get(column);
        if (!actual.isStarter()){
            actual.setValue(0);
            actualState--;
        }

    }

    public void setSingleBoardValue(int row, int column, int value){
        if (value <= values.get(values.size()-1) && value >= 0){
            GameValue gv = new GameValue(value);
            board.get(row).set(column, gv);
            actualState++;
        }
        else{
            System.out.println("something went wrong");
        }
    }
    public boolean checkWinning(){
        return actualState==boardSize;
    }

    public String toString(){
        String result = "";
        for (ArrayList<GameValue> ar:
                board) {
            result += ar.toString() + "\n";
        }
        return result;
    }

    public List<ArrayList<GameValue>> getBoard() {
        return board;
    }

    public List<Integer> getValues() {
        return values;
    }
}
